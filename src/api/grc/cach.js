import request from '@/utils/request'


// 查询缓存sql详细
export function getCachlist(id) {
  return request({
    url: '/grc/cach/listDate/' + id,
    method: 'get'
  })
}

export function clearCachelist(id) {
  return request({
    url: '/grc/cach/clearCache/' + id,
    method: 'get'
  })
}


// 查询缓存sql详细
export function getGrcCachlist(id) {
  return request({
    url: '/grc/cach/listDate/' + id,
    method: 'get'
  })
}



// 查询缓存sql列表
export function listCach(query) {
  return request({
    url: '/grc/cach/list',
    method: 'get',
    params: query
  })
}

// 查询缓存sql详细
export function getCach(id) {
  return request({
    url: '/grc/cach/' + id,
    method: 'get'
  })
}

// 新增缓存sql
export function addCach(data) {
  return request({
    url: '/grc/cach',
    method: 'post',
    data: data
  })
}

// 修改缓存sql
export function updateCach(data) {
  return request({
    url: '/grc/cach',
    method: 'put',
    data: data
  })
}

// 删除缓存sql
export function delCach(id) {
  return request({
    url: '/grc/cach/' + id,
    method: 'delete'
  })
}

// 导出缓存sql
export function exportCach(query) {
  return request({
    url: '/grc/cach/export',
    method: 'get',
    params: query
  })
}
