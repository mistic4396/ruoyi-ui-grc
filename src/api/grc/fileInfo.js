import request from '@/utils/request'

// 查询附件信息列表
export function listFileInfo(query) {
  return request({
    url: '/grc/fileInfo/list',
    method: 'get',
    params: query
  })
}

// 查询附件信息详细
export function getDownloadInfo(fileId) {
  return request({
    url: '/grc/fileInfo/download/' + fileId,
    method: 'get'
  })
}


// 查询附件信息详细
export function getFileInfo(fileId) {
  return request({
    url: '/grc/fileInfo/' + fileId,
    method: 'get'
  })
}

// 新增附件信息
export function addFileInfo(data) {
  return request({
    url: '/grc/fileInfo',
    method: 'post',
    data: data
  })
}

// 修改附件信息
export function updateFileInfo(data) {
  return request({
    url: '/grc/fileInfo',
    method: 'put',
    data: data
  })
}

// 删除附件信息
export function delFileInfo(fileId) {
  return request({
    url: '/grc/fileInfo/' + fileId,
    method: 'delete'
  })
}

// 导出附件信息
export function exportFileInfo(query) {
  return request({
    url: '/grc/fileInfo/export',
    method: 'get',
    params: query
  })
}