import request from '@/utils/request'

// 查询损失明细列表
export function listItem(query) {
  return request({
    url: '/grc/item/list',
    method: 'get',
    params: query
  })
}

// 查询损失明细详细
export function getItem(id) {
  return request({
    url: '/grc/item/' + id,
    method: 'get'
  })
}

// 新增损失明细
export function addItem(data) {
  return request({
    url: '/grc/item',
    method: 'post',
    data: data
  })
}

// 修改损失明细
export function updateItem(data) {
  return request({
    url: '/grc/item',
    method: 'put',
    data: data
  })
}

// 删除损失明细
export function delItem(id) {
  return request({
    url: '/grc/item/' + id,
    method: 'delete'
  })
}

// 导出损失明细
export function exportItem(query) {
  return request({
    url: '/grc/item/export',
    method: 'get',
    params: query
  })
}