import request from '@/utils/request'

// 查询编号生成列表
export function listSeq(query) {
  return request({
    url: '/grc/seq/list',
    method: 'get',
    params: query
  })
}

// 查询编号生成详细
export function getSeq(id) {
  return request({
    url: '/grc/seq/' + id,
    method: 'get'
  })
}

// 新增编号生成
export function addSeq(data) {
  return request({
    url: '/grc/seq',
    method: 'post',
    data: data
  })
}

// 修改编号生成
export function updateSeq(data) {
  return request({
    url: '/grc/seq',
    method: 'put',
    data: data
  })
}

// 删除编号生成
export function delSeq(id) {
  return request({
    url: '/grc/seq/' + id,
    method: 'delete'
  })
}

// 导出编号生成
export function exportSeq(query) {
  return request({
    url: '/grc/seq/export',
    method: 'get',
    params: query
  })
}