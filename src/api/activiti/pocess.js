import request from '@/utils/request'




// 查询流程列表
export function listPocess(query) {
  return request({
    url: '/activiti/pocess/list',
    method: 'get',
    params: query
  })
}

// 查询流程详细
export function getPocess(pocessId) {
  return request({
    url: '/activiti/pocess/' + pocessId,
    method: 'get'
  })
}


// 获取历史信息
export function getProcessHistoric(processInstanceId) {
  return request({
    url: '/activiti/instance/getProcessHistoric/' + processInstanceId,
    method: 'get'
  })
}
// 获取历史信息
export function getProcessHistoricBYbusinessId(businessId,befinitionKey) {
  return request({
    url: '/activiti/instance/getProcessHistoricBYbusinessId/' + businessId+'/'+befinitionKey,
    method: 'get'
  })
}



// 启动时获取下一个节点
export function startNextGrcUserTask(befinitionKey) {
  return request({
    url: '/activiti/instance/startNext/' + befinitionKey,
    method: 'get'
  })
}

// 启动流程
export function startPoces(data) {
  return request({
    url: 'activiti/instance/start',
    method: 'post',
    data: data
  })
}

// 提交时获取下一个节点
export function getNextUserTaskList(taskId) {
  return request({
    url: '/activiti/instance/getNextUserTaskList/' + taskId,
    method: 'get'
  })
}

// 提交时
export function complete(data) {
  return request({
    url: 'activiti/instance/complete',
    method: 'post',
    data: data
  })
}




// 新增流程
export function addPocess(data) {
  return request({
    url: '/activiti/pocess',
    method: 'post',
    data: data
  })
}

// 修改流程
export function updatePocess(data) {
  return request({
    url: '/activiti/pocess',
    method: 'put',
    data: data
  })
}

// 删除流程
export function delPocess(pocessId) {
  return request({
    url: '/activiti/pocess/' + pocessId,
    method: 'delete'
  })
}



// 导出流程
export function exportPocess(query) {
  return request({
    url: '/activiti/pocess/export',
    method: 'get',
    params: query
  })
}
