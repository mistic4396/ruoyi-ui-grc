import request from '@/utils/request'

// 查询损失事件列表
export function listLossevent(query) {
  return request({
    url: '/system/lossevent/list',
    method: 'get',
    params: query
  })
}


// 查询损失事件列表
export function vueParmiter(data) {
  return request({
    url: '/activiti/instance/vueParmiter',
    method: 'post',
     data: data
  })
}


// 查询损失事件详细
export function getLossevent(id) {
  return request({
    url: '/system/lossevent/' + id,
    method: 'get'
  })
}

// 新增损失事件
export function addLossevent(data) {
  return request({
    url: '/system/lossevent',
    method: 'post',
    data: data
  })
}

// 修改损失事件
export function updateLossevent(data) {
  return request({
    url: '/system/lossevent',
    method: 'put',
    data: data
  })
}

// 删除损失事件
export function delLossevent(id) {
  return request({
    url: '/system/lossevent/' + id,
    method: 'delete'
  })
}

// 导出损失事件
export function exportLossevent(query) {
  return request({
    url: '/system/lossevent/export',
    method: 'get',
    params: query
  })
}
