import request from '@/utils/request'

// 查询KRI指标信息列表
export function listInfo(query) {
  return request({
    url: '/kri/Info/list',
    method: 'get',
    params: query
  })
}

// 查询KRI指标信息详细
export function getInfo(id) {
  return request({
    url: '/kri/Info/' + id,
    method: 'get'
  })
}

// 新增KRI指标信息
export function addInfo(data) {
  return request({
    url: '/kri/Info',
    method: 'post',
    data: data
  })
}

// 修改KRI指标信息
export function updateInfo(data) {
  return request({
    url: '/kri/Info',
    method: 'put',
    data: data
  })
}

// 删除KRI指标信息
export function delInfo(id) {
  return request({
    url: '/kri/Info/' + id,
    method: 'delete'
  })
}

// 导出KRI指标信息
export function exportInfo(query) {
  return request({
    url: '/kri/Info/export',
    method: 'get',
    params: query
  })
}