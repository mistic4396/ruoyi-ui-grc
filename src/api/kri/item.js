import request from '@/utils/request'

// 查询KRI指标明细列表
export function listItem(query) {
  return request({
    url: '/kri/item/list',
    method: 'get',
    params: query
  })
}



// 查询KRI指标明细详细
export function getItem(id) {
  return request({
    url: '/kri/item/' + id,
    method: 'get'
  })
}

// 新增KRI指标明细
export function addItem(data) {
  return request({
    url: '/kri/item',
    method: 'post',
    data: data
  })
}

// 修改KRI指标明细
export function updateItem(data) {
  return request({
    url: '/kri/item',
    method: 'put',
    data: data
  })
}

// 删除KRI指标明细
export function delItem(id) {
  return request({
    url: '/kri/item/' + id,
    method: 'delete'
  })
}

// 导出KRI指标明细
export function exportItem(query) {
  return request({
    url: '/kri/item/export',
    method: 'get',
    params: query
  })
}